# Portable development environment

Have your IDE and dev environment follow your code.
In the codebase, the .devenv directory holds your project's
development environment.

* Checked in files:
  * docker-compose.yml and Dockerfile used to define one or more containers for your environment
  * list of vscode extensions needed by the project
  * custom scripts to run each time a container is created, in case modifications are needed
* git-ignored in files:
  * config = the IDE container's home directory, used to save personal changes (ie .bashrc)

## Requirements
- docker
- docker-compose --build

## Usage
```
cd .devenv
docker-compose up
```

## Files checked in with the project

### .devenv/Dockerfile
Docker file used to create the dev environment. Here, you can install the necessary applications and tools.

### .devenv/extensions.txt
List of VSCode extensions used for this project. They will be installed when docker-compose up is run

### .devenv/.env
Environment variables to control the VSCode server. These vars include:

|Variable Name|Notes|Example|
|-------------|-----|-------|
|PASSWORD|password log into vscode server| ***** |
|SUDO_PASSWORD|password for user to sudo| ***** |
|GIT_USER_NAME|info for git config|Bob Roberts|
|GIT_USER_EMAIL|info for git config|bobbobs@mymail.com|
|VSCODE_USER_EXTENSIONS|User specific extensions|"mhutchie.git-graph https://open-vsx.org/api/vscodevim/vim/1.16.0/file/vscodevim.vim-1.16.0.vsix"|
|VSCODE_USER_SETTINGS|User specific vscode settings|'"extensions.autoUpdate": false,"workbench.startupEditor": "newUntitledFile","terminal.integrated.shell.linux": "/bin/bash","git.suggestSmartCommit": false, "workbench.editorAssociations": [], "explorer.confirmDelete": false'|

### .devenv/config
Mapped to the IDE user's home directory. This is useful for keep ssh keys, bashrc configuraitons, etc.

### .devenv/custom-cont-init.d
Directory storing scripts that run when the container is created. Scripts include setting git user information, vscode user settings, install missing extensions, etc.

Note: The scripts run as root on startup. There is a check to run them as the user if necessary.
