#!/bin/bash
if test "$(whoami)" == "root"
then
  echo "Running as root... exiting"
  exit
fi

export HOME=$(eval echo ~$(whoami))
