#!/bin/bash
if test "$(whoami)" != "root"
then
  echo "Not running as root... exiting"
  exit
fi

USER="abc"

SCRIPT_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" > /dev/null 2>&1 && pwd )"

for SCRIPT in ${SCRIPT_DIR}/*; do
  NAME="$(basename "${SCRIPT}")"
  if [ -f "${SCRIPT}" ]; then
    echo "[custom-init] ${NAME}: executing as $USER..."
    sudo -E -u $USER /bin/bash ${SCRIPT}
    echo "[custom-init] ${NAME}: exited $?"
  elif [ ! -f "${SCRIPT}" ]; then
    echo "[custom-init] ${NAME}: is not a file"
  fi
done
