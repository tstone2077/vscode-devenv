#!/bin/bash

SCRIPT_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" > /dev/null 2>&1 && pwd )"
. $SCRIPT_DIR/check-root.sh
EXT_DIR=${HOME}/extensions

extensions=$(cat "$SCRIPT_DIR/../extensions.txt" | sed -e 's/#.*//' | xargs echo)

getExtFromPath() {
    echo ${1##*/} | sed 's/-[0-9\.]\+\.vsix//'
}

getFileFromURL() {
  echo ${1##*/}
}

processConfig() {
  type=$1
  e=$2

  if echo $e | grep "^http[s]:" > /dev/null
  then
    ext=$(getExtFromPath $e)
    key="/tmp/$(getFileFromURL $e)"
    url=$e
  else
    ext=$e
    key=$ext
  fi
  if ! echo $currentExts | grep $ext > /dev/null 2>&1
  then
    installed="-"
  else
    installed="+"
  fi
  if test -n $url && test $installed == '-'
  then
      echo "Downloading extension: $ext"
      curl -sLo "$key" "$url"
  fi
  allExts[$key]="$installed,$type,$ext,$url"
}

# "installed?,from,extension,url"
declare -A allExts
echo Getting currently installed extensions...
currentExts=$(code-server --extensions-dir=$EXT_DIR --list-extensions)

for e in $extensions
do
  processConfig "proj" "$e"
done

for e in $VSCODE_USER_EXTENSIONS
do
  processConfig "env" "$e"
done

echo -e "installed\tfrom\textension"
echo -e "---------\t----\t---------"
for key in ${!allExts[*]}
do
  echo "${allExts[$key]}" | while IFS=, read -r installed type ext url
  do
    echo -e "$installed        \t$type\t$ext"
    if test "$installed" != '*' && \
       test -n "$url"
    then
      filename=$(getFileFromURL $url)
    fi
  done
done

toInstall=""
for key in ${!allExts[*]}
do
  data=${allExts[$key]}
  if test "${data:0:1}" != '+'
  then
    code-server --extensions-dir $EXT_DIR --force --install-extension $key
    toInstall+=" $key"
  fi
done
