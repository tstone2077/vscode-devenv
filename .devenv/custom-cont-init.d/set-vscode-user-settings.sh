#!/bin/bash

# For example:
#   export VSCODE_USER_SETTINGS='"workbench.colorTheme": "Visual Studio Dark"'

SCRIPT_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" > /dev/null 2>&1 && pwd )"
. $SCRIPT_DIR/check-root.sh

if ! test -z "$VSCODE_USER_SETTINGS"
then
    echo "Saving user settings ..."
    USER_ROOT=${HOME}/data/User/
    mkdir -p $USER_ROOT
    USER_SETTINGS_FILE=$USER_ROOT/settings.json
    if test -e "$USER_SETTINGS_FILE"
    then
        cat $USER_SETTINGS_FILE | jq ". += {$VSCODE_USER_SETTINGS}" > /tmp/settings.json
    else
        echo '{}' | jq ". += {$VSCODE_USER_SETTINGS}" > /tmp/settings.json
    fi
    cat /tmp/settings.json
    mv /tmp/settings.json $USER_SETTINGS_FILE
fi
