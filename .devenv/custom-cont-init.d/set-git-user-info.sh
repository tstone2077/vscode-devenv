#!/bin/bash

SCRIPT_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" > /dev/null 2>&1 && pwd )"
. $SCRIPT_DIR/check-root.sh

if ! test -e ${HOME}/.gitconfig
then
    if ! test -z "$GIT_USER_NAME" && ! test -z "$GIT_USER_EMAIL"
    then
      echo "Setting git user information"
      cat > ${HOME}/.gitconfig <<EOF
[user]
    name = "$GIT_USER_NAME"
    email = "$GIT_USER_EMAIL"
EOF
    fi
fi
